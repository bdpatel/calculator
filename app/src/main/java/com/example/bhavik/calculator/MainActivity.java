package com.example.bhavik.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnone,btntwo,btnthree,btnfour,btnfive,btnsix,btnseven,btneight,btnnine,btnzero,
            btnpraOpen,btnpraClose,btnSqrt,btnPd,btnEql,btnadd,btnsub,btndiv,btnmul,btnClear;

    TextView txtview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtview = findViewById(R.id.txtView);

        btnzero = findViewById(R.id.btn0);
        btnzero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"0");
            }
        });

        btnone = findViewById(R.id.btn1);
        btnone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"1");
            }
        });

        btntwo = findViewById(R.id.btn2);
        btntwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"2");
            }
        });

        btnthree = findViewById(R.id.btn3);
        btnthree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"3");
            }
        });

        btnfour = findViewById(R.id.btn4);
        btnfour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"4");
            }
        });

        btnfive = findViewById(R.id.btn5);
        btnfive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"5");
            }
        });

        btnsix = findViewById(R.id.btn6);
        btnsix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"6");
            }
        });

        btnseven = findViewById(R.id.btn7);
        btnseven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"7");
            }
        });

        btneight = findViewById(R.id.btn8);
        btneight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"8");
            }
        });

        btnnine = findViewById(R.id.btn9);
        btnnine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"9");
            }
        });


        btnPd = findViewById(R.id.btnpd);
        btnPd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+".");
            }
        });

        //btnEql = findViewById(R.id.btneql);
        //btnEql.setOnClickListener(new View.OnClickListener() {
            //@Override
          //  public void onClick(View view) {
                //txtview.setText(txtview.getText()+"=");
           // }
        //});

        btnadd = findViewById(R.id.btnAdd);
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"+");
            }
        });

        btnsub = findViewById(R.id.btnSub);
        btnsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"-");
            }
        });

        btndiv = findViewById(R.id.btnDiv);
        btndiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"/");
            }
        });

        btnmul = findViewById(R.id.btnMul);
        btnmul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"*");
            }
        });

        btnClear = findViewById(R.id.btnclear);
        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText("");
            }
        });

        btnpraOpen = findViewById(R.id.btnpraopen);
        btnpraOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+"(");
            }
        });

        btnpraClose = findViewById(R.id.btnpraclose);
        btnpraClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+")");
            }
        });

        btnSqrt = findViewById(R.id.btnsqrt);
        btnSqrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtview.setText(txtview.getText()+" √");
            }
        });
    }
}
